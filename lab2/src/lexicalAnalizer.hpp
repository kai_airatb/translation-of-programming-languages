#pragma once

#include <iostream>
#include <string>
#include <string.h>

typedef enum s_charType
{
	ct_eot,
	ct_eol,
	ct_letter,
	ct_number,
	ct_special,
	ct_space,
	ct_unknown
} e_charType;

typedef enum s_wordType
{
	wt_nothing,
	wt_word,
	wt_number
} e_wordType;

typedef enum s_workState
{
	ws_finish,
	ws_start,
	ws_continue
} e_workState;

class LexicalAnalizer
{
public:
	LexicalAnalizer	(std::string text);
	~LexicalAnalizer();

	e_charType		getNextSymbol();
	e_wordType		getNextWord();
	e_workState		getWorkState();
	char			getCurrentChar();
	e_charType		getCurrentCharType();
	std::string		getCurrentWord();
	e_wordType		getCurrentWordType();

protected:
	int				row;
	int				col;
	e_workState		ws;
	std::string		input;
	std::size_t		inputLen;
	std::size_t		position;
	char			currentChar;
	e_charType		currentCharType;
	std::string		currentWord;
	e_wordType		currentWordType;

	e_wordType		firstWord();
	e_wordType		secondWord();
};
