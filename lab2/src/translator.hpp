#pragma once

#include <iostream>
#include <string>

class Translator
{
public:
	Translator(std::string input_text);
	~Translator();
	std::string getLog();

protected:
	std::string input;
	std::string log;
};
