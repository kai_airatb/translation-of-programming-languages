#include "translator.hpp"
#include "lexicalAnalizer.hpp"

Translator::Translator(std::string text)
{
	this->input = text;
	LexicalAnalizer la = LexicalAnalizer(text);
	try
	{
		while (la.getWorkState())
		{
			switch (la.getNextWord())
			{
			case wt_number:
				log += "[num<" + la.getCurrentWord() + ">]";
				break;
			case wt_word:
				log += "[wrd<" + la.getCurrentWord() + ">]";
				break;
			}
		}
	}
	catch (std::string msg)
	{
		log += "[";
		log += la.getCurrentWord();
		log += "{err: ";
		log += msg;
		log += "}]";
	}
}

Translator::~Translator()
{
}

std::string Translator::getLog()
{
	return (log);
}

