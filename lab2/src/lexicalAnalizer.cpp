#include "lexicalAnalizer.hpp"

LexicalAnalizer::LexicalAnalizer(std::string text)
{
	this->input = text;
	this->position = -1;
	this->inputLen = text.length();
	this->ws = ws_start;
	this->row = 1;
	this->col = 0;
}

LexicalAnalizer::~LexicalAnalizer()
{
}

char		LexicalAnalizer::getCurrentChar() {return (currentChar);}
e_charType	LexicalAnalizer::getCurrentCharType() {return (currentCharType);}
std::string	LexicalAnalizer::getCurrentWord() {return (currentWord);}
e_wordType	LexicalAnalizer::getCurrentWordType() {return (currentWordType);}
e_workState	LexicalAnalizer::getWorkState() {return (ws);}

e_charType LexicalAnalizer::getNextSymbol()
{
	if (ws == ws_finish)
		return (currentCharType);
	col += 1;
	position += 1;
	if (inputLen <= position && !(ws = ws_finish))
	{
		currentChar = '\0';
		return ((currentCharType = ct_eot));
	}
	ws = ws_continue;
	currentChar = input[position];
	if ('a' <= currentChar && currentChar <= 'd')
		return ((currentCharType = ct_letter));
	if ('0' <= currentChar && currentChar <= '1')
		return ((currentCharType = ct_number));
	if (strchr("({[<>]})*\\/!@\"#$:;%^&*`~", currentChar))
		return ((currentCharType = ct_special));
	if (strchr("\t ", currentChar))
		return ((currentCharType = ct_space));
	if ('\n' == currentChar)
	{
		col = 0;
		row += 1;
		return ((currentCharType = ct_eol));
	}
	return ((currentCharType = ct_unknown));
}

e_wordType LexicalAnalizer::getNextWord()
{
	currentWord = "";
	do
	{
		getNextSymbol();
	} while (currentCharType == ct_space || currentCharType == ct_eol);
	if (!currentCharType)
		return (wt_nothing);
	if (currentCharType == ct_number)
		return (firstWord());
	if (currentCharType == ct_letter)
		return (secondWord());
	return (wt_nothing);
}

bool isInArr(int n, int *arr)
{
	if (n == -1)
		return (false);
	while (*arr != -1 && *arr != n)
		++arr;
	return (*arr == n);
}

bool isSeparatorChar(e_charType c)
{
	if (c == ct_space
	||  c == ct_special
	||  c == ct_eot
	||  c == ct_eol)
		return (true);
	return (false);
}

std::string genErrMsg(int state, char *label, int *expected, int *fin)
{
	int i = 0;
	int n = 0;
	int len = strlen(label);
	std::string msg = "Далее ожидалось";

	while (i < len)
	{
		if (expected[i] != -1)
		{
			if (n != 0)
				msg += " или";
			msg += " '";
			msg += label[i];
			msg += "'";
			++n;
		}
		++i;
	}
	if (isInArr(state, fin))
	{
		if (n != 0)
			msg += " или";
		msg += " конец слова";
	}
	return (msg);
}

e_wordType LexicalAnalizer::firstWord()
{
	int next = 0;
	int state = 0;
	char name[] = "01";
	int wm[][2] = {
		{  1, -1 },  // 0 -- A
		{  2, -1 },  // 1 -- B
		{  0,  3 },  // 2 -- C
		{  4, -1 },  // 3 -- DFin
		{ -1,  5 },  // 4 -- E
		{  3, -1 }}; // 5 -- F
	int fin[] = {3, -1};
	do {
		state = next;
		if (currentCharType == ct_number)
		{
			next = wm[state][currentChar - name[0]];
			if (next == -1)
				break;
			currentWord += currentChar;
			getNextSymbol();
		}
		else
			break;
	} while (true);
	if (isInArr(state, fin) && isSeparatorChar(currentCharType))
		return (wt_number);
	throw (genErrMsg(state, name, wm[state], fin));
	return (wt_nothing);
}

e_wordType LexicalAnalizer::secondWord()
{
	int next = 0;
	int state = 0;
	char name[] = "abcd";
	int wm[][4] = {
		{  1, -1, -1, -1},  // 0 -- AFin
		{  1,  2, -1, -1},  // 1 -- BFin
		{ -1,  2,  3, -1},  // 2 -- CFin
		{ -1, -1,  3,  4},  // 3 -- DFin
		{ -1, -1, -1,  4}}; // 4 -- EFin
	int fin[] = {1, 2, 3, 4, -1};
	do {
		state = next;
		if (currentCharType == ct_letter)
		{
			next = wm[state][currentChar - name[0]];
			if (next == -1)
				break;
			currentWord += currentChar;
			getNextSymbol();
		}
		else
			break;
	} while (true);
	if (isInArr(state, fin) && isSeparatorChar(currentCharType))
		return (wt_word);
	throw (genErrMsg(state, name, wm[state], fin));
	return (wt_nothing);
}
