#pragma once

#include <iostream>
#include <string>
#include "wordList.hpp"
#include "treeView.hpp"

class Translator
{
public:
	Translator();
	~Translator();
	void setInput(std::string);
	bool search(std::string);
	void doMagic();
	std::string getLog();
	std::string getWords();
	std::string getTreeView();

protected:
	TreeView tv;
	WordList wl;
	std::string input;
	std::string log;
	std::string words;
};
