#include "translator.hpp"
#include "lexicalAnalizer.hpp"

Translator::Translator(std::string text)
{
	this->input = text;
	LexicalAnalizer la = LexicalAnalizer(text);
	charType ct;
	while (la.getWorkState())
	{
		switch (la.getNextSymbol())
		{
		case ct_eol:
			log += "[eol]\n";
			break;
		case ct_eot:
			log += "[EOT]";
			break;
		case ct_letter:
			log += "[let]";
			break;
		case ct_number:
			log += "[num]";
			break;
		case ct_special:
			log += "[&&&]";
			break;
		case ct_space:
			log += "[spc]";
			break;
		case ct_unknown:
			log += "[???]";
			break;
		}
	}
}

Translator::~Translator()
{
}

std::string Translator::getLog()
{
	return (log);
}
