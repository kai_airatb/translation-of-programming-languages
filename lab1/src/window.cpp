#include "window.hpp"
#include "translator.hpp"

Window::Window()
:	m_VBox(Gtk::Orientation::VERTICAL),
	m_HBox(Gtk::Orientation::HORIZONTAL),
	m_ButtonBox(Gtk::Orientation::VERTICAL),
	m_Button_Clear("Clear", true),
	m_Button_Run("Run")
{
	set_title("Lab");
	set_default_size(800, 600);

	m_VBox.set_margin(5);
	m_VBox.set_spacing(5);
	m_HBox.set_spacing(5);
	m_ButtonBox.set_spacing(5);

	// Input
	m_InputTextView.set_monospace();
	m_InputScrolledWindow.set_child(m_InputTextView);
	m_InputScrolledWindow.set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::AUTOMATIC);
	m_InputScrolledWindow.set_expand();
	m_InputScrolledWindow.set_vexpand();

	// Output
	m_OutputTextView.set_monospace();
	m_OutputTextView.set_editable(false);
	m_OutputScrolledWindow.set_child(m_OutputTextView);
	m_OutputScrolledWindow.set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::AUTOMATIC);
	m_OutputScrolledWindow.set_expand();
	m_OutputScrolledWindow.set_vexpand();

	//Connect signals:
	m_Button_Clear.signal_clicked().connect(sigc::mem_fun(*this, &Window::on_button_clear));
	m_Button_Run.signal_clicked().connect(sigc::mem_fun(*this, &Window::on_button_run));

	// append
	m_ButtonBox.append(m_Button_Run);
	m_ButtonBox.append(m_Button_Clear);
	m_HBox.append(m_InputScrolledWindow);
	m_HBox.append(m_ButtonBox);
	m_VBox.append(m_HBox);
	m_VBox.append(m_OutputScrolledWindow);
	set_child(m_VBox);

	// Get text buffers
	inBuf = m_InputTextView.get_buffer();
	outBuf = m_OutputTextView.get_buffer();
}

Window::~Window()
{
}

void Window::on_button_clear()
{
	inBuf->set_text("");
	outBuf->set_text("");
}

void Window::on_button_run()
{
	std::string text = inBuf->get_text();
	Translator translator = Translator(text);
	outBuf->set_text(translator.getLog());
}
