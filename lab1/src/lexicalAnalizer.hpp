#pragma once

#include <iostream>
#include <string>
#include <string.h>

typedef enum e_charType
{
	ct_eot,
	ct_eol,
	ct_letter,
	ct_number,
	ct_special,
	ct_space,
	ct_unknown
} charType;

typedef enum e_workState
{
	ws_finish,
	ws_start,
	ws_continue
} workState;

class LexicalAnalizer
{
public:
	LexicalAnalizer(std::string text);
	~LexicalAnalizer();
	charType getNextSymbol();
	workState getWorkState();
	int _row;
	int _col;

protected:
	std::string input;
	std::size_t inputLen;
	std::size_t position;
	workState ws;
};
