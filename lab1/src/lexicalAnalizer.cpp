#include "lexicalAnalizer.hpp"

LexicalAnalizer::LexicalAnalizer(std::string text)
{
	this->input = text;
	this->position = -1;
	this->inputLen = text.length();
	this->ws = ws_start;
	this->_row = 1;
	this->_col = 0;
}

LexicalAnalizer::~LexicalAnalizer()
{
}

charType LexicalAnalizer::getNextSymbol()
{
	position += 1;
	_col += 1;
	if (ws == ws_finish || (inputLen <= position && !(ws = ws_finish)))
		return (ct_eot);
	ws = ws_continue;
	if ('a' <= input[position] && input[position] <= 'd')
		return (ct_letter);
	if ('0' <= input[position] && input[position] <= '1')
		return (ct_number);
	if (strchr("({[<>]})*\\/!@\"#$:;%^&*`~", input[position]))
		return (ct_special);
	if (strchr("\t ", input[position]))
		return (ct_space);
	if ('\n' == input[position])
	{
		_col = 0;
		_row += 1;
		return (ct_eol);
	}
	return (ct_unknown);
}

workState LexicalAnalizer::getWorkState()
{
	return (ws);
}
