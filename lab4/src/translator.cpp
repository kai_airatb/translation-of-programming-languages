#include "translator.hpp"
#include "syntaxAnalizer.hpp"
#include "wordList.hpp"

Translator::Translator()
{
}

void Translator::setInput(std::string text)
{
	this->input = text;
}

void Translator::doMagic()
{
	SyntaxAnalizer sa = SyntaxAnalizer(this->input);
	log = "";
	words = "";
	if (sa.checkSyntax())
	{
		log += "[Все верно]";
		wl.setInput(this->input);
		wl.read();
		words += "first type\n";
		words += wl.getFirst();
		words += "\n";
		words += "second type\n";
		words += wl.getSecond();
	}
	else
	{
		log += sa.getReaded();
		log += "[";
		log += sa.getErrMsg();
		log += "]";
	}
}

Translator::~Translator()
{
}

std::string Translator::getLog()
{
	return (log);
}

std::string Translator::getWords()
{
	return (words);
}

bool Translator::search(std::string str)
{
	return (wl.search(str));
}
