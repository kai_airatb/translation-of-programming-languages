#include "syntaxAnalizer.hpp"
#include "lexicalAnalizer.hpp"

SyntaxAnalizer::SyntaxAnalizer(std::string text)
{
	la.setInput(text);
	this->err = false;
}

SyntaxAnalizer::~SyntaxAnalizer()
{
}

std::string SyntaxAnalizer::getErrMsg() {return (this->errMsg);}
std::string SyntaxAnalizer::getReaded() {return (this->readed);}

bool SyntaxAnalizer::setErr(std::string msg)
{
	if (err == false)
	{
		this->err = true;
		this->errMsg = msg;
		this->readed = la.getReaded();
	}
	return (this->err);
}

// S → <2>(A) := B;
bool SyntaxAnalizer::checkSyntax()
{
	try
	{
		la.getNextWord();
		S();
		while (la.getNextNonSpaceSymbol() != ct_eot)
		{
			la.getNextWord();
			S();
		}
	}
	catch (const char* e)
	{
		return (!setErr(e));
	}
	return (!this->err);
}

void SyntaxAnalizer::S()
{
	if (la.getCurrentWordType() == wt_word)
	{
		if (la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == '(')
		{
			la.getNextSymbol();
			A();
		}
		else
			throw ("Ожидалось '('");
		if (la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ')')
		{
			la.getNextSymbol();
			if (la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ':')
			{
				if (la.getNextSymbol() == ct_special && la.getCurrentChar() == '=')
				{
					la.getNextSymbol();
					B();
				}
				else
					throw ("Ожидалось '='");
				if (!err && la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ';')
				{
					la.getNextWord();
				}
				else
					throw ("Ожидалось ',' или ';'");
			}
			else
				throw ("Ожидалось ':'");
		}
		else
			throw ("Ожидалось число или ')'");
	}
	else
		throw ("Ожидалось слово");
}

// A → <1> | <1> A
void SyntaxAnalizer::A()
{
	if (la.getNextWord() == wt_number)
	{
		la.getNextNonSpaceSymbol();
		if (la.getCurrentChar() == ')')
			return ;
		A();
	}
	else
		throw ("Ожидалось число");
}

// B → <2> | <2>C
void SyntaxAnalizer::B()
{
	if (la.getNextWord() == wt_word)
	{
		if (la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ';')
			return ;
		C();
	}
	else
		throw ("Ожидалось слово");
}

// C → ,<2> | ,<2>C
void SyntaxAnalizer::C()
{
	if (la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ',')
	{
		la.getNextSymbol();
		if (la.getNextWord() == wt_word)
		{
			if (la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ',')
			{
				C();
			}
		}
		else
			throw ("Ожидалось слово");
	}
}
