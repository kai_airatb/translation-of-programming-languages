#pragma once

#include <iostream>
#include <string>
#include <string.h>

#include "lexicalAnalizer.hpp"
#include "hashTable.hpp"

class WordList
{
public:
	WordList ();
	~WordList();

	void setInput(std::string text);
	void read				();
	std::string getFirst	();
	std::string getSecond	();
	bool search(std::string str);

protected:
	std::string				input;
	HashTable				one;
	HashTable				two;
};
