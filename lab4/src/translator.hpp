#pragma once

#include <iostream>
#include <string>
#include "wordList.hpp"

class Translator
{
public:
	Translator();
	~Translator();
	void setInput(std::string);
	bool search(std::string);
	void doMagic();
	std::string getLog();
	std::string getWords();

protected:
	WordList wl;
	std::string input;
	std::string log;
	std::string words;
};
