#include <gtk/gtk.h>
#include <iostream>

#include "translator.hpp"

GtkBuilder *builder;
GtkWidget *window;
GtkWidget *input;
GtkWidget *output1;
GtkWidget *output2;
GtkWidget *search;
GtkWidget *run;
Translator t;

void
do_search(GtkEditable *editable, gpointer data)
{
	std::string str = gtk_editable_get_chars(editable, 0, -1);
	if (t.search(str))
		str += " found!\n";
	else
		str = "";
	gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(output1)), str.c_str(), -1);
}

std::string get_input()
{
	GtkTextIter start_iter;
	GtkTextIter end_iter;

	GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(input));
	gtk_text_buffer_get_start_iter(buf, &start_iter);
	gtk_text_buffer_get_end_iter(buf, &end_iter);
	std::string s = gtk_text_buffer_get_text(buf, &start_iter, &end_iter, FALSE);
	return (s);
}

void run_click(GtkToggleButton *button, gpointer data)
{
	gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(output1)), "", -1);
	gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(output2)), "", -1);
	t.setInput(get_input());
	t.doMagic();
	gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(output1)), t.getTreeView().c_str(), -1);
	gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(output2)), t.getLog().c_str(), -1);
}

static GtkWidget*
create_window (void)
{
	GError* error = NULL;

	builder = gtk_builder_new();
	if (!gtk_builder_add_from_file (builder, "ui.glade", &error))
	{
		g_critical("Не могу загрузить файл: %s", error->message);
		g_error_free(error);
	}
	window	= GTK_WIDGET(gtk_builder_get_object(builder, "window"));
	input	= GTK_WIDGET(gtk_builder_get_object(builder, "input"));
	output1	= GTK_WIDGET(gtk_builder_get_object(builder, "output1"));
	output2	= GTK_WIDGET(gtk_builder_get_object(builder, "output2"));
	search	= GTK_WIDGET(gtk_builder_get_object(builder, "search"));
	run		= GTK_WIDGET(gtk_builder_get_object(builder, "run"));
	g_signal_connect(G_OBJECT(run), "clicked", G_CALLBACK(run_click), NULL);
	g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
	g_signal_connect(G_OBJECT(search), "changed", G_CALLBACK(do_search), NULL);
	gtk_builder_connect_signals(builder, NULL);
	// set line by default
	gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(input)), "aaa(001 001):=aab, abccc, abc;", -1);
	if (!window)
		g_critical("Ошибка при получении виджета окна");
	g_object_unref(builder);
	return window;
}

int main(int argc, char** argv) {
	GtkWidget *window;
	gtk_init(&argc, &argv);
	window = create_window();
	gtk_widget_show(window);
	gtk_main();
	return (0);
}
