#include "syntaxAnalizer.hpp"
#include "lexicalAnalizer.hpp"

SyntaxAnalizer::SyntaxAnalizer(std::string text)
{
	la.setInput(text);
	this->err = false;
}

SyntaxAnalizer::~SyntaxAnalizer()
{
}

std::string SyntaxAnalizer::getErrMsg() {return (this->errMsg);}
std::string SyntaxAnalizer::getReaded() {return (this->readed);}

bool SyntaxAnalizer::setErr(std::string msg)
{
	if (err == false)
	{
		this->err = true;
		this->errMsg = msg;
		this->readed = la.getReaded();
	}
	return (this->err);
}

// S → <2>(A) := B;
bool SyntaxAnalizer::checkSyntax()
{
	index = 0;
	try
	{
		la.getNextWord();
		S();
		while (la.getNextNonSpaceSymbol() != ct_eot)
		{
			la.getNextWord();
			S();
		}
	}
	catch (const char* e)
	{
		return (!setErr(e));
	}
	return (!this->err);
}

void SyntaxAnalizer::S()
{
	tv.add_inside("S" + std::to_string(index));
	tv.go_in("S" + std::to_string(index));
	index++;
	if (la.getCurrentWordType() == wt_word)
	{
		if (identifiers.search(la.getCurrentWord()))
			throw ("Слово уже встречалось");
		identifiers.add(la.getCurrentWord());
		tv.add_inside(la.getCurrentWord());
		if (la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == '(')
		{
			tv.add_inside("(");
			la.getNextSymbol();
			A();
		}
		else
			throw ("Ожидалось '('");
		if (la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ')')
		{
			tv.add_inside(")");
			la.getNextSymbol();
			if (la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ':')
			{
				if (la.getNextSymbol() == ct_special && la.getCurrentChar() == '=')
				{
					tv.add_inside(":=");
					la.getNextSymbol();
					B();
				}
				else
					throw ("Ожидалось '='");
				if (!err && la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ';')
				{
					tv.add_inside(";");
					la.getNextWord();
				}
				else
					throw ("Ожидалось ',' или ';'");
			}
			else
				throw ("Ожидалось ':'");
		}
		else
			throw ("Ожидалось число или ')'");
	}
	else
		throw ("Ожидалось слово");
	tv.go_out();
}

// A → <1> | <1> A
void SyntaxAnalizer::A()
{
	tv.add_inside("A");
	tv.go_in("A");
	if (la.getNextWord() == wt_number)
	{
		tv.add_inside(la.getCurrentWord());
		la.getNextNonSpaceSymbol();
		if (la.getCurrentChar() == ')')
		{
			tv.go_out();
			return ;
		}
		A();
	}
	else
		throw ("Ожидалось число");
	tv.go_out();
}

// B → <2> | <2>C
void SyntaxAnalizer::B()
{
	tv.add_inside("B");
	tv.go_in("B");
	if (la.getNextWord() == wt_word)
	{
		if (identifiers.search(la.getCurrentWord()))
			throw ("Слово уже встречалось");
		identifiers.add(la.getCurrentWord());
		tv.add_inside(la.getCurrentWord());
		if (la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ';')
		{
			tv.go_out();
			return ;
		}
		C();
	}
	else
		throw ("Ожидалось слово");
	tv.go_out();
}

// C → ,<2> | ,<2>C
void SyntaxAnalizer::C()
{
	tv.add_inside("C");
	tv.go_in("C");
	if (la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ',')
	{
		tv.add_inside(",");
		la.getNextSymbol();
		if (la.getNextWord() == wt_word)
		{
			if (identifiers.search(la.getCurrentWord()))
				throw ("Слово уже встречалось");
			identifiers.add(la.getCurrentWord());
			tv.add_inside(la.getCurrentWord());
			if (la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ',')
			{
				C();
			}
		}
		else
			throw ("Ожидалось слово");
	}
	tv.go_out();
}
