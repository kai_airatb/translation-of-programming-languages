#include "translator.hpp"
#include "syntaxAnalizer.hpp"
#include "wordList.hpp"
#include "treeView.hpp"

Translator::Translator()
{
	this->err = true;
}

void Translator::setInput(std::string text)
{
	this->input = text;
	this->err = true;
}

void Translator::doMagic()
{
	SyntaxAnalizer sa = SyntaxAnalizer(this->input);
	log = "";
	words = "";
	if (sa.checkSyntax())
	{
		printf("ok\n");
		log += "[Все верно]";
		wl.setInput(this->input);
		wl.read();
		words += "first type\n";
		words += wl.getFirst();
		words += "\n";
		words += "second type\n";
		words += wl.getSecond();
		err = false;
	}
	else
	{
		printf("err\n");
		log += sa.getReaded();
		log += "[";
		log += sa.getErrMsg();
		log += "]";
		err = true;
	}
	tv = sa.tv;
}

Translator::~Translator()
{
}

std::string Translator::getLog()
{
	return (log);
}

std::string Translator::getWords()
{
	if (err)
		return ("");
	return (words);
}

bool Translator::search(std::string str)
{
	if (err)
		return (false);
	return (wl.search(str));
}

std::string Translator::getTreeView()
{
	if (err)
		return ("");
	return (tv.to_string());
}
