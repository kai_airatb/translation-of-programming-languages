#include "generator.hpp"

Generator::Generator(treeNode *tv)
{
    this->tv = tv;
    make_struct();
    lines = new Lines();
}

void Generator::make_struct()
{
    if (!tv)
        return ;
    if (tv->name[0] == 'S')
        S(lines);
    if (tv->name[0] == 'A')
        A(line);
    if (tv->name[0] == 'B')
        B(line);
    if (tv->name[0] == 'C')
        C(line);
}

void Generator::S(Container *c)
{
    line = new Line();
    c->push_back(line);
    if (tv->inside)
    {
        tv = tv->inside;
        make_struct();
        tv = tv->outside;
    }
    if (tv->next)
    {
        tv = tv->next;
        Args *a = new Args();
    }
}
void Generator::A(Container *c)
{
    if (!tv)
        return ;
    Enum *en = new Enum();
    c->push_back(en);
    treeNode *ptr;
    ptr = tv;
    while (ptr)
    {
        if (ptr->name[0] != 'A')
        {
            Num *n = new Num();
            n->value = 666;
            c->push_back(n);
            ptr = ptr->next;
        }
        else
            ptr = ptr->inside;
    }
    tv = tv->next;
    make_struct();
}
void Generator::B(Container *c)
{
    if (!tv)
        return ;
    Enum *en = new Enum();
    c->push_back(en);


    tv = tv->next;
    make_struct();
}
void Generator::C(Container *c)
{

}

Generator::~Generator(){}

std::string Generator::to_string()
{
    return (lines->to_string());
}
