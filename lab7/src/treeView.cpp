#include "treeView.hpp"

TreeView::TreeView()
{
	home = new treeNode({"home", NULL, NULL, NULL});
	ptr = home;
	as_string = "";
}

TreeView::~TreeView()
{
}

void TreeView::add_inside(std::string name)
{
	treeNode *i;

	if (ptr->inside == NULL)
	{
		ptr->inside = new treeNode({name, NULL, NULL, ptr});
		return ;
	}
	i = ptr->inside;
	while (i->next != NULL)
		i = i->next;
	i->next = new treeNode({name, NULL, NULL, ptr});
}

void TreeView::add_next(std::string name)
{
	treeNode *i;

	i = ptr;
	while (i->next != NULL)
		i = i->next;
	i->next = new treeNode({name, NULL, NULL, i->outside});
}

bool TreeView::go_in(std::string name)
{
	treeNode *back;

	if (home == NULL)
		return (false);
	if (ptr == NULL)
		ptr = home;
	back = ptr;
	ptr = ptr->inside;
	while (ptr != NULL && ptr->name != name)
		ptr = ptr->next;
	if (ptr == NULL)
		ptr = back;
	return (ptr != back);
}

bool TreeView::go_next()
{
	if (ptr->next != NULL)
		ptr = ptr->next;
	else
		return (false);
	return (true);
}

bool TreeView::go_out()
{
	if (ptr->outside != NULL)
		ptr = ptr->outside;
	else
		return (false);
	return (true);
}

void TreeView::go_home()
{
	ptr = home;
}

std::string TreeView::to_string(treeNode *h)
{
	if (h == NULL)
		return (as_string);
	as_string += " {\n";
	++ind;
	while (h)
	{
		as_string += indent(ind);
		if (!h->inside)
			as_string += "\"";
		as_string += h->name;
		if (!h->inside)
			as_string += "\",\n";
		else
			as_string += ":";
		to_string(h->inside);
		h = h->next;
	}
	as_string += indent(--ind) + "},\n";
	return (as_string);
}

std::string TreeView::to_string()
{
	treeNode *h;

	h = home;
	ind = 0;
	while (h)
	{
		as_string += indent(ind);
		if (!h->inside)
			as_string += "\"";
		as_string += h->name;
		if (!h->inside)
			as_string += "\",\n";
		else
			as_string += ":";
		to_string(h->inside);
		h = h->next;
	}
	return (as_string);
}

std::string TreeView::indent(int n)
{
	std::string out;

	out = "";
	for (int i = 0; i < n; i++)
	{
		out += "\t";
	}
	return (out);
}

treeNode *TreeView::get_home_node()
{
	return (home);
}
