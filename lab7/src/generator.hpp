#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <string.h>

#include "treeView.hpp"

class Item
{
public:
    std::string to_string();
};
class Container: public Item, public std::vector<Item*>
{
public:
    std::string to_string();
};

class Enum: public Container
{
public:
    std::string separator = ",";
    std::string to_string()
    {
        return "enum";
    };
};
class Lines: public Container
{
public:
    std::string to_string()
    {
        std::string str = "";
        for (int i = 0; i < this->size(); i++)
        {
            str += this[i].to_string();
            str += ";\n";
        }
        return (str);
    };
};
class Line: public Container
{
public:
    std::string to_string()
    {
        std::string str = "";
        for (int i = 0; i < this->size(); i++)
        {
            str += this[i].to_string();
        }
        return (str);
    };
};

class Var: public Item
{
public:
    std::string name = "";
    std::string to_string()
    {
        return "var";
    };
};
class Num: public Item
{
public:
    int value = 0;
    std::string to_string()
    {
        return "num";
    };
};
class Args: public Item
{
public:
    Container args;
    std::string to_string()
    {
        std::string str  = "(";
        str += args.to_string();
        str += ")";
        return (str);
    };
};
class Assign: public Item
{
public:
    Item args[2];
    std::string to_string()
    {
        std::string str = args[0].to_string();
        str += " := ";
        str += args[1].to_string();
        return (str);
    };
};

/*
aaa(001 001):=aab, abccc, abc;

var aaa
args ()
    enum
        number 001
        number 001
enum
    var aab
    var abccc
    var abc
*/

class Generator
{
private:
    Lines *lines = NULL;
    Line *line = NULL;
    treeNode *tv = NULL;
    void make_struct();
public:
	Generator (treeNode *tv);
	~Generator();

    std::string to_string();
    void S(Container *c);
    void A(Container *c);
    void B(Container *c);
    void C(Container *c);
};
