#include "wordList.hpp"
#include "lexicalAnalizer.hpp"

WordList::WordList()
{
}

void WordList::setInput(std::string text)
{
	this->input = text;
	one.clear();
	two.clear();
}

WordList::~WordList() {}

void WordList::read()
{
	LexicalAnalizer la;
	la.setInput(this->input);
	while (la.getWorkState() != ws_finish)
	{
		switch (la.getNextWord())
		{
			case wt_word:
				one.add(la.getCurrentWord());
				break;
			case wt_number:
				two.add(la.getCurrentWord());
				break;
		}
	}
}

bool WordList::search(std::string str)
{
	return (one.search(str) || two.search(str));
}

std::string WordList::getFirst()
{
	return (one.printAll());
}

std::string WordList::getSecond()
{
	return (two.printAll());
}
