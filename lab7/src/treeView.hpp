#pragma once

#include <iostream>
#include <string>

typedef struct s_treeNode treeNode;

struct s_treeNode
{
	std::string name;
	treeNode *next;
	treeNode *inside;
	treeNode *outside;
};


class TreeView
{
public:	
	treeNode *home;
	treeNode *ptr;
	TreeView();
	~TreeView();
	treeNode *get_home_node();
	std::string to_string();
	std::string to_string(treeNode *h);
	void add_inside(std::string content);
	void add_next(std::string content);
	bool go_in(std::string content);
	bool go_next();
	bool go_out();
	void go_home();

private:
	std::string as_string;
	int ind = 0;

	std::string indent(int n);
};
