#include "hashTable.hpp"

HashTable::HashTable()
{
	for (int i = 0; i < 128; ++i)
	{
		list[i] = NULL;
	}
}

HashTable::~HashTable() {}

void HashTable::clear()
{
	for (int i = 0; i < 128; ++i)
	{
		list[i] = NULL;
	}
}

// sdbm hash function with mod 128
int HashTable::hash(std::string s)
{
	char *str = strdup(s.c_str());
	char *tofree = str;
	unsigned long hash = 0;
	int c;

	while (c = *str++)
		hash = c + (hash << 6) + (hash << 16) - hash;
	free(tofree);
	return (hash % 128);
}

wordlist *HashTable::new_list(std::string str)
{
	wordlist *list;

	list = (wordlist *)malloc(sizeof(wordlist));
	list->next = NULL;
	list->str = strdup(str.c_str());
	return (list);
}

void HashTable::add(std::string str)
{
	wordlist *ptr;
	int h = hash(str);

	if (list[h] == NULL)
		list[h] = new_list(str);
	else
	{
		ptr = list[h];
		while (ptr->next != NULL && ptr->str != str)
			ptr = ptr->next;
		if (ptr->str == str)
			return ;
		ptr->next = new_list(str);
	}
}

bool HashTable::search(std::string str)
{
	wordlist *ptr;
	int h = hash(str);

	if (list[h] == NULL)
		return (false);
	ptr = list[h];
	while (ptr->next != NULL && ptr->str != str)
		ptr = ptr->next;
	if (ptr->str == str)
		return (true);
	return (false);
}

std::string HashTable::printAll()
{
	std::string str = "";
	wordlist *ptr;

	for (int i = 0; i < 128; i++)
	{
		ptr = list[i];
		if (ptr != NULL)
		{
			str += "[";
			str += std::to_string(i);
			str += "]: ";
			str += ptr->str;
			while ((ptr = ptr->next) != NULL)
			{
				str += " -> ";
				str += ptr->str;
			}
			str += "\n";
		}
	}
	return (str);
}
