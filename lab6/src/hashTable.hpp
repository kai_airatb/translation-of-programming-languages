#pragma once

#include <iostream>
#include <string>
#include <string.h>

typedef struct s_wordlist wordlist;

struct s_wordlist
{
	char *str;
	wordlist *next;
};

class HashTable
{
public:
	HashTable ();
	~HashTable();

	void add(std::string);
	void clear();
	bool search(std::string);
	//bool delete(std::string);
	std::string printAll();

protected:
	wordlist *list[128];

	int hash(std::string);
	wordlist *new_list(std::string str);
};
