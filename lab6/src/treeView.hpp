#pragma once

#include <iostream>
#include <string>

typedef struct s_treeNode treeNode;

struct s_treeNode
{
	std::string name;
	treeNode *next;
	treeNode *inside;
	treeNode *outside;
};


class TreeView
{
public:	
	TreeView();
	~TreeView();
	std::string to_string();
	std::string to_string(treeNode *h);
	void add_inside(std::string content);
	void add_next(std::string content);
	void go_in(std::string content);
	void go_out();
	void go_home();

private:
	treeNode *home;
	treeNode *ptr;
	std::string as_string;
	int ind = 0;

	std::string indent(int n);
};
