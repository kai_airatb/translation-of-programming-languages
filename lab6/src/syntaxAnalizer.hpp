#pragma once

#include <iostream>
#include <string>
#include <string.h>

#include "treeView.hpp"
#include "hashTable.hpp"
#include "lexicalAnalizer.hpp"

class SyntaxAnalizer
{
public:
	SyntaxAnalizer	(std::string text);
	~SyntaxAnalizer	();

	bool checkSyntax		();
	std::string getErrMsg	();
	std::string getReaded	();
	TreeView				tv;

protected:
	LexicalAnalizer			la;
	std::string				errMsg;
	std::string				readed;
	int						index;
	bool					err;
	bool					setErr(std::string msg);

	void					A();
	void					B();
	void					C();
	void					S();

	HashTable				identifiers;
};
