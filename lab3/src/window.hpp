#pragma once

#include <gtkmm.h>
#include <string>

class Window : public Gtk::Window
{
public:
	Window();
	virtual ~Window();
	void on_button_run();

protected:

	//Signal handlers:
	void on_button_clear();

	//Child widgets:
	Gtk::Box m_VBox;
	Gtk::Box m_HBox;
	Gtk::Box m_ButtonBox;

	Gtk::ScrolledWindow m_InputScrolledWindow;
	Gtk::TextView m_InputTextView;

	Gtk::ScrolledWindow m_OutputScrolledWindow;
	Gtk::TextView m_OutputTextView;

	Gtk::Button m_Button_Clear, m_Button_Run;

	Glib::RefPtr<Gtk::TextBuffer> inBuf, outBuf;
};
