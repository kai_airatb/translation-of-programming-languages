#include "translator.hpp"
#include "syntaxAnalizer.hpp"

Translator::Translator(std::string text)
{
	this->input = text;
	SyntaxAnalizer sa = SyntaxAnalizer(text);
	if (sa.checkSyntax())
	{
		log += "[Все верно]";
	}
	else
	{
		log += sa.getReaded();
		log += "[";
		log += sa.getErrMsg();
		log += "]";
	}
}

Translator::~Translator()
{
}

std::string Translator::getLog()
{
	return (log);
}
