#pragma once

#include <iostream>
#include <string>
#include <string.h>

#include "lexicalAnalizer.hpp"

class SyntaxAnalizer
{
public:
	SyntaxAnalizer	(std::string text);
	~SyntaxAnalizer	();

	bool checkSyntax		();
	std::string getErrMsg	();
	std::string getReaded	();

protected:
	LexicalAnalizer			la;
	std::string				errMsg;
	std::string				readed;
	bool					err;
	bool					setErr(std::string msg);

	bool					A();
	bool					B();
	bool					C();
};
