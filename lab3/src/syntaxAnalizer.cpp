#include "syntaxAnalizer.hpp"
#include "lexicalAnalizer.hpp"

SyntaxAnalizer::SyntaxAnalizer(std::string text)
{
	la.setInput(text);
	this->err = false;
}

SyntaxAnalizer::~SyntaxAnalizer()
{
}

std::string SyntaxAnalizer::getErrMsg() {return (this->errMsg);}
std::string SyntaxAnalizer::getReaded() {return (this->readed);}

bool SyntaxAnalizer::setErr(std::string msg)
{
	if (err == false)
	{
		this->err = true;
		this->errMsg = msg;
		this->readed = la.getReaded();
	}
	return (this->err);
}

// S → <2>(A) := B;
bool SyntaxAnalizer::checkSyntax()
{
	try
	{
		if (la.getNextWord() == wt_word)
		{
			if (!err && la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == '(')
			{
				la.getNextSymbol();
				if (A())
					return (!err);
			}
			else
				return (!setErr("Ожидалось '('"));
			if (!err && la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ')')
			{
				la.getNextSymbol();
				if (!err && la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ':')
				{
					if (!err && la.getNextSymbol() == ct_special && la.getCurrentChar() == '=')
					{
						la.getNextSymbol();
						if (B())
							return (!err);
					}
					else
						return (!setErr("Ожидалось '='"));
					if (!err && la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ';')
						return (!this->err);
					else
						return (!setErr("Ожидалось ',' или ';'"));
				}
				else
					return (!setErr("Ожидалось ':'"));
			}
			else
				return (!setErr("Ожидалось число или ')'"));
		}
		else
			return (!setErr("Ожидалось слово"));
	}
	catch (std::string e)
	{
		return (!setErr("Неправильное слово"));
	}
	return (!this->err);
}

// A → <1> | <1> A
bool SyntaxAnalizer::A()
{
	if (!err && la.getNextWord() == wt_number)
	{
		la.getNextNonSpaceSymbol();
		if (la.getCurrentChar() == ')')
			return (this->err);
		if (!err && A())
		{
			errMsg = "Ожидалось число или ')'";
			return (this->err);
		}
	}
	else
		return (setErr("Ожидалось число"));
	return (this->err);
}

// B → <2> | <2>C
bool SyntaxAnalizer::B()
{
	if (!err && la.getNextWord() == wt_word)
	{
		if (!err && la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ';')
			return (this->err);
		else
		{
			if (!err && C())
				return (this->err);
		}
	}
	else
		return (setErr("Ожидалось слово"));
	return (this->err);
}

// C → ,<2> | ,<2>C
bool SyntaxAnalizer::C()
{
	if (!err && la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ',')
	{
		la.getNextSymbol();
		if (!err && la.getNextWord() == wt_word)
		{
			if (!err && la.getNextNonSpaceSymbol() == ct_special && la.getCurrentChar() == ',')
			{
				if (!err && C())
					return (this->err);
			}
			else
				return (this->err);
		}
		else
			return (setErr("Ожидалось слово"));
	}
	return (this->err);
}
